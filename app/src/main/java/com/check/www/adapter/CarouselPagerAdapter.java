package com.check.www.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.check.www.R;
import com.check.www.data.LotteryData;
import com.check.www.http.RetrofitHelper;

import java.util.List;


public class CarouselPagerAdapter extends PagerAdapter {

    Context context;
    List<LotteryData.Data.Prize> data;

    public CarouselPagerAdapter(Context context, List<LotteryData.Data.Prize> data) {
        this.context = context;
        this.data = data;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_carousel, null);
        try {

            ImageView imageCover =  view.findViewById(R.id.img_carousel);

            Glide.with(context).load(data.get(position).getPrizePic()).into(imageCover);
            container.addView(view);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return (view == object);
    }

}