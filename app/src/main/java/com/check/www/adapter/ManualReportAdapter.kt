package com.check.www.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.check.www.R
import com.check.www.data.ManualReportData
import com.check.www.data.body.TokenBody
import com.check.www.http.RetrofitHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.cell_manualreport_item.view.*
import java.text.SimpleDateFormat
import java.util.*


fun <T : RecyclerView.ViewHolder> T.listen(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(adapterPosition, itemViewType)
    }
    return this
}
private var dataSource: List<ManualReportData.Data.People> = mutableListOf()

class ManualReportAdapter(
    private val context: Context?
) : RecyclerView.Adapter<ManualReportAdapter.mViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): mViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_manualreport_item, parent, false)
        return mViewHolder(view).listen { position, type ->
            val item = dataSource.get(position)
            onConfirmClick?.invoke(item)

        }
    }

    var onConfirmClick: ((ManualReportData.Data.People) -> Unit?)? = null


    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holderM: mViewHolder, position: Int) {
        holderM.bind(dataSource[position])
    }

    inner class mViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        @SuppressLint("SetTextI18n")
        fun bind(data: ManualReportData.Data.People) {


            if(data.checkinStatus == 0){

                itemView.tv_date.text ="未報到"
                itemView.imageView4.setImageResource(R.drawable.manual_buttom_green)

                itemView.imageView4.setOnClickListener {

                    RetrofitHelper(context as FragmentActivity?).setCallback {
                        if (it.isSuccess) {
                            Toast.makeText(context, "報到成功", Toast.LENGTH_LONG).show()
                            itemView.imageView4.setImageResource(R.drawable.manual_buttom_gray)
                            val date = Date()
                            val dateTimeFormatter = SimpleDateFormat("HH:mm")
                            val dateAnswer: String = dateTimeFormatter.format(date)
                            itemView.tv_date.text ="$dateAnswer 報到成功"
                        } else {
                            Toast.makeText(context, "報到成功", Toast.LENGTH_LONG).show()
                        }
                    }.checkIn(data.pcode)

                }
            }
           else{
                itemView.imageView4.setImageResource(R.drawable.manual_buttom_gray)
                itemView.tv_date.text ="${data.checkinTS}報到成功"
            }


            itemView.tv_jobTitle.text = data.unit
            itemView.tv_number.text = data.pcode
            itemView.tv_name.text = data.name



        }

    }

    fun setManualReportData(manualReportData: List<ManualReportData.Data.People>) {
        dataSource = manualReportData
    }



}