package com.check.www.adapter

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.check.www.R
import com.check.www.activity_report.ScanActivity
import com.check.www.data.ActivityReportData

import com.check.www.data.ManualReportData
import com.check.www.lottery.StartLotteryActivity
import kotlinx.android.synthetic.main.cell_activityreport_item.view.*


fun <T : RecyclerView.ViewHolder> T.listens(event: (position: Int, type: Int) -> Unit): T {
    itemView.setOnClickListener {
        event.invoke(getAdapterPosition(), getItemViewType())
    }
    return this
}

class ActivityReportAdapter(
    private val context: Context?,
    private val dataSource: MutableList<ActivityReportData>
) : RecyclerView.Adapter<ActivityReportAdapter.ViewHolder>() {
    private var onItemClickListener: OnItemClickListener? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_activityreport_item, parent, false)
        return ViewHolder(view).listens { position, type ->
            val item = dataSource.get(position)
            onConfirmClick?.invoke(item)

        }
    }

    var onConfirmClick: ((ActivityReportData) -> Unit?)? = null


    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holderM: ViewHolder, position: Int) {
        holderM.bind(dataSource[position],position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: ActivityReportData,position: Int) {

            itemView.tv_name.text = data.name
            itemView.tv_title.text = data.title
            itemView.img.setImageResource(data.img)

            itemView.btn_enter.setOnClickListener {
                onItemClickListener?.onEnterClick(position)

            }


        }

    }


    interface OnItemClickListener {
        fun onEnterClick(position: Int)

    }

    fun setOnItemClickListener(listener: OnItemClickListener) {
        this.onItemClickListener = listener
    }


}