package com.check.www.adapter

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.check.www.R
import com.check.www.data.LotteryData
import com.check.www.data.LotteryWinnerData
import com.check.www.data.ManualReportData
import kotlinx.android.synthetic.main.cell_lottery_item.view.*


class LotteryAdapter(
    private val context: Context?,
    private val dataSource: List<LotteryData.Data.Prize.Winner>
) : RecyclerView.Adapter<LotteryAdapter.mViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): mViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_lottery_item, parent, false)
        return mViewHolder(view).listen { position, _ ->
            val item = dataSource[position]
            onConfirmClick?.invoke(item)

        }
    }

    var onConfirmClick: ((LotteryData.Data.Prize.Winner) -> Unit?)? = null


    override fun getItemCount(): Int {
        return dataSource.size
    }

    override fun onBindViewHolder(holder: mViewHolder, position: Int) {
        holder.bind(dataSource[position],position)
    }

    inner class mViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(data: LotteryData.Data.Prize.Winner,position: Int) {

            if((position %2)==0){

                itemView.ll_winning_bg.setBackgroundColor(Color.parseColor("#e2e2e2"))

            }else{

                itemView.ll_winning_bg.setBackgroundColor(Color.parseColor("#f6f6f6"))

            }

            itemView.tv_name.text = data.name
            itemView.tv_number.text = data.pcode
        }

    }


}