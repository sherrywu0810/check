package com.check.www.data

import java.io.Serializable
import com.google.gson.annotations.SerializedName





data class ManualReportData(
    @SerializedName("data")
    var data: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Int = 0
):Serializable {
    data class Data(
        @SerializedName("people")
        var people: List<People> = listOf()
    ) :Serializable{
        data class People(
            @SerializedName("checkinStatus")
            var checkinStatus: Int = 0,
            @SerializedName("checkinTS")
            var checkinTS: String = "",
            @SerializedName("department")
            var department: String = "",
            @SerializedName("name")
            var name: String = "",
            @SerializedName("pcode")
            var pcode: String = "",
            @SerializedName("title")
            var title: String = "",
            @SerializedName("unit")
            var unit: String = ""
        ):Serializable
    }
}