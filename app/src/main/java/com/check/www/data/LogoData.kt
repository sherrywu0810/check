package com.check.www.data


import com.google.gson.annotations.SerializedName

data class LogoData(
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Double = 0.0
) {
    data class Data(
        @SerializedName("logo")
        var logo: String = ""
    )
}