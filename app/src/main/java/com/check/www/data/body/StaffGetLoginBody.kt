package com.check.www.data.body

data class StaffGetLoginBody(

    var username: String,
    var password: String

){
    companion object {
        fun getEmpty(): StaffGetLoginBody {
            return StaffGetLoginBody("", "")
        }
    }


}
