package com.check.www.data

import com.google.gson.annotations.SerializedName


data class LotteryWinnerData(
    @SerializedName("data")
    var data: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Double = 0.0
) {
    data class Data(
        @SerializedName("winners")
        var winners: List<Winner> = listOf()
    ) {
        data class Winner(
            @SerializedName("name")
            var name: String = "",
            @SerializedName("pcode")
            var pcode: String = ""
        )
    }
}