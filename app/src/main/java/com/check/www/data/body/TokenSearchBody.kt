package com.check.www.data.body

import com.google.gson.annotations.SerializedName

data class SearchBody(
    @SerializedName("text")
    var text: String = ""

)