package com.check.www.data

data class MemberAccessToken(
    var access_token: String,
    var expires_in: Double,
    var refresh_token: String,
    var scope: String,
    var token_type: String
)