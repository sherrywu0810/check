package com.check.www.data

data class Club(
    var address: String,
    var id: Int,
    var lati: String,
    var longi: String,
    var name: String,
    var number_at_club: Int,
    var phone: String
)