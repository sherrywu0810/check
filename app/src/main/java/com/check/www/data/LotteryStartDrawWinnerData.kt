package com.check.www.data

import java.io.Serializable
import com.google.gson.annotations.SerializedName



data class LotteryStartDrawWinnerData(
    @SerializedName("data")
    var data: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Double = 0.0
) {
    data class Data(
        @SerializedName("draw")
        var draw: Draw = Draw()
    ) {
        data class Draw(
            @SerializedName("numDrawn")
            var numDrawn: Int = 0,
            @SerializedName("numWinners")
            var numWinners: Int = 0,
            @SerializedName("warn")
            var warn: String = "",
            @SerializedName("winners")
            var winners: List<Winner> = listOf()
        ) {
            data class Winner(
                @SerializedName("name")
                var name: String = "",
                @SerializedName("pcode")
                var pcode: String = ""
            )
        }
    }
}