package com.check.www.data
import com.google.gson.annotations.SerializedName


data class PeopleGetSearch(
    @SerializedName("data")
    var data: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("hash")
    var hash: String = "",
    @SerializedName("result")
    var result: Int = 0,
    @SerializedName("time")
    var time: Long = 0
) {
    data class Data(
        @SerializedName("people")
        var people: List<People> = listOf()
    ) {
        data class People(
            @SerializedName("checkinStatus")
            var checkinStatus: Int = 0,
            @SerializedName("checkinTime")
            var checkinTime: String = "",
            @SerializedName("deparment")
            var deparment: String = "",
            @SerializedName("name")
            var name: String = "",
            @SerializedName("pcode")
            var pcode: String = "",
            @SerializedName("title")
            var title: String = "",
            @SerializedName("unit")
            var unit: String = ""
        )
    }
}