package com.check.www.data

import com.google.gson.annotations.SerializedName

data class LoginData(
    @SerializedName("data")
    var data: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Int = 0
) {
    data class Data(
        @SerializedName("token")
        var token: String = ""
    )
}