package com.check.www.data
import com.google.gson.annotations.SerializedName


data class PeopleGetCheckIn(
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Double = 0.0
) {
    data class Data(
        @SerializedName("person")
        var person: Person = Person()
    ) {
        data class Person(
            @SerializedName("checkinStatus")
            var checkinStatus: Double = 0.0,
            @SerializedName("checkinTS")
            var checkinTS: String = "",
            @SerializedName("department")
            var department: String = "",
            @SerializedName("firstTime")
            var firstTime: Boolean = false,
            @SerializedName("id")
            var id: String = "",
            @SerializedName("name")
            var name: String = "",
            @SerializedName("pcode")
            var pcode: String = "",
            @SerializedName("title")
            var title: String = "",
            @SerializedName("unit")
            var unit: String = ""
        )
    }
}