package com.check.www.data.body

import com.google.gson.annotations.SerializedName

data class TokenBody(

    @SerializedName("token")
    var token: String = ""

)