package com.check.www.data

import com.check.www.R
import java.io.Serializable




data class ActivityReportData(
    var name: String="",
    var title: String="",
    var img: Int = 0,
    var type: Int = onces


): Serializable{

    companion object{
        const val onces = 0
        const val continuous = 1

    }

}


