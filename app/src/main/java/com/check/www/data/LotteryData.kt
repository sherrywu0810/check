package com.check.www.data

import java.io.Serializable
import com.google.gson.annotations.SerializedName



data class LotteryData(
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Double = 0.0
) {
    data class Data(
        @SerializedName("prizes")
        var prizes: List<Prize> = listOf()
    ) {
        data class Prize(
            @SerializedName("id")
            var id: String = "",
            @SerializedName("numDrawn")
            var numDrawn: Int = 0,
            @SerializedName("numWinners")
            var numWinners: Int = 0,
            @SerializedName("prizeContent")
            var prizeContent: String = "",
            @SerializedName("prizeOrder")
            var prizeOrder: Int = 0,
            @SerializedName("prizePic")
            var prizePic: String = "",
            @SerializedName("prizeType")
            var prizeType: String = "",
            @SerializedName("prizename")
            var prizename: String = "",
            @SerializedName("winners")
            var winners: List<Winner> = listOf()
        ){
            data class Winner(
                @SerializedName("name")
                var name: String = "",
                @SerializedName("pcode")
                var pcode: String = ""
            )
        }
    }
}