package com.check.www.data
import com.google.gson.annotations.SerializedName




data class ActivityInfoData(
    @SerializedName("data")
    var `data`: Data = Data(),
    @SerializedName("errormsg")
    var errormsg: String = "",
    @SerializedName("result")
    var result: Int = 0
) {
    data class Data(
        @SerializedName("event")
        var event: Event = Event()
    ) {
        data class Event(
            @SerializedName("custName")
            var custName: String = "",
            @SerializedName("eventCode")
            var eventCode: String = "",
            @SerializedName("eventName")
            var eventName: String = "",
            @SerializedName("id")
            var id: String = "",
            @SerializedName("logoPic")
            var logoPic: String = "",
            @SerializedName("numCheckin")
            var numCheckin: Int = 0,
            @SerializedName("numTotal")
            var numTotal: Int = 0,
            @SerializedName("percent")
            var percent: Float = 0.0f,
            @SerializedName("themePic")
            var themePic: String = ""
        )
    }
}