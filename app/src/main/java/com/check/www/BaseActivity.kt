package com.check.www

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Toast
import com.check.www.utility.PrefHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_base.*

open class BaseActivity : AppCompatActivity() {


    companion object {
        const val TAG = "cacaca"
        const val keyData = "keyData"
    }


    lateinit var preHelper: PrefHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        super.setContentView(R.layout.activity_base)

//        FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
//            instanceIdResult ->
//            val Token = instanceIdResult.token
//            Log.d("sherry", Token)
//        }

        preHelper = PrefHelper(this)


    }

    fun setHippoTitle(titleId:Int){
        fl_base_top_bar.visibility = View.VISIBLE
        tv_base_title.setText(titleId)

    }

    fun intentTo(java: Class<*>) {
        val intent = Intent()
        intent.setClass(this,java)
        startActivity(intent)
    }

    fun intentToAndClearAll(java: Class<*>) {
        val intent = Intent()
        intent.setClass(this,java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)

        startActivity(intent)
    }

    fun toJson(any:Any):String{
        return Gson().toJson(any)
    }

    fun getCell(_id:Int):View{
        return layoutInflater.inflate(_id,null)
    }

    fun setContentView(layoutId:Int, isCenterView:Boolean){
        if(isCenterView){
            layoutInflater.inflate(layoutId,ll_center_container)
        }else{
            layoutInflater.inflate(layoutId,ll_container)
        }
    }


    fun toast(_id:Int){
        toast(getString(_id))
    }
    fun toast(msg:String){
        Toast.makeText(this,msg,Toast.LENGTH_LONG).show()
    }



    fun showLoading(){
        fl_base_loading.visibility = View.VISIBLE
    }
    fun hideLoading(){
        fl_base_loading.visibility = View.GONE
    }

    override fun setContentView(layoutId:Int){
        setContentView(layoutId,false)
    }

    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object: TypeToken<T>() {}.type)



}
