package com.check.www.http

import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v4.app.FragmentActivity
import android.util.Log
import com.check.www.BuildConfig
import com.check.www.data.body.SearchBody
import com.check.www.data.body.StaffGetLoginBody
import com.check.www.data.body.TokenBody
import com.check.www.utility.PrefHelper
import com.google.gson.Gson
import okhttp3.RequestBody
import okio.Buffer
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import java.io.IOException


class RetrofitHelper(val activity: FragmentActivity?) {

    class ResponseData {
        var isSuccess: Boolean = false
        var dataBodyJsonString: String = ""
        var errorMessage: String = ""
        var error: String = ""
        var errorBody: String = ""

    }

    private val hardCodeToken =
        "Basic a2hjMk9YWFk1OGdlZHFCMmxiWEZMdlFreW9iSWlJWHhRak9nTlNXdzowemc5emZKTlNLbWJOVVFZczNMRGVnYlI4REJ1RHlVRmRwbHd6REtRUlB3Wmk3RUtHcnlLM08yMlBTaGkxMVdjMEQxc1FOUjZZQnNQWFRBd0JhMzdHSVZSNWo4VnFlYkxidXhvV0tWRW9FbjFnbmtXc2laNDRrRThqQW9qSk9yeg=="
    private var apiService: ApiService
    private var responseCallback: ((responseData: ResponseData) -> Unit)? = null
    private var callback = object : Callback<Any> {


        override fun onFailure(call: Call<Any>, t: Throwable) {
            Log.d("cacaca", "onFailure:", t)

            val responseData = ResponseData()
            t.message.toString()
            responseData.isSuccess = false
            responseData.errorMessage = "Server Busy!"
            responseData.error = t.message.toString()
            responseCallback?.invoke(responseData)
        }

        @RequiresApi(Build.VERSION_CODES.N)
        override fun onResponse(call: Call<Any>, response: Response<Any>) {
            val responseData = ResponseData()
            responseData.errorBody =
                if (response.errorBody() == null) response.message() else response.errorBody()!!.string()

            if (BuildConfig.DEBUG) {
                Log.d("cacaca", "---------------------------------")
                Log.d("cacaca", "call method:" + call.request().method())
                Log.d("cacaca", "response code:" + response.code())
                Log.d("cacaca", "error body:" + responseData.errorBody)
                Log.d("cacaca", "call url:" + call.request().url())
                Log.d("cacaca", "call request body-->:" + bodyToString(call.request().body()))
                Log.d("cacaca", "call response body<--:" + Gson().toJson(response.body()))
                Log.d("cacaca", "call Authorization:" + call.request().header("Authorization"))
                Log.d("cacaca", "---------------------------------")
            }

            responseData.isSuccess = response.isSuccessful
            responseData.dataBodyJsonString = Gson().toJson(response.body())
            responseData.errorMessage = response.message()

            responseCallback?.invoke(responseData)
        }

    }

    companion object {

        const val apiBase = BuildConfig.SEVER_URL

    }

    init {
        val retrofit = Retrofit.Builder()
            .addConverterFactory(NullOnEmptyConverterFactory())
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(apiBase)
            .build()

        apiService = retrofit.create(ApiService::class.java)
    }

    fun setCallback(function: (responseData: ResponseData) -> Unit): RetrofitHelper {
        responseCallback = function
        return this
    }


    fun getEventLogo(evCode: String) {
        apiService.getEventLogo(evCode).enqueue(callback)
    }


    fun getStaffLogin(staffLogin: StaffGetLoginBody) {
        apiService.getStaffLogin(staffLogin).enqueue(callback)
    }

    fun getLuckyDrawLogin(staffLogin: StaffGetLoginBody) {
        apiService.getLuckyDrawLogin(staffLogin).enqueue(callback)
    }

    fun getActivityInfo() {
        apiService.getActivityInfo(getEventCode()).enqueue(callback)
    }

    fun getSearchInfo(searchBody: SearchBody) {
        apiService.getSearchInfo(getEventId(), searchBody).enqueue(callback)
    }

    fun getPrizeList() {
        apiService.getPrizeList(getEventId()).enqueue(callback)
    }

    fun checkIn(people_id: String) {
        apiService.checkIn(getEventId(), people_id, getToken()).enqueue(callback)
    }

    fun getPrizeWinner(prizeId: String) {
        apiService.getPrizeWinner(getEventId(), prizeId).enqueue(callback)
    }

    fun getPrizeDraw(prizeId: String) {
        apiService.getPrizeDraw(getEventId(), prizeId, getPrizeToken()).enqueue(callback)
    }

    fun getPrizeDrawAll(prizeId: String) {
        apiService.getPrizeDrawAll(getEventId(), prizeId, getPrizeToken()).enqueue(callback)
    }

    private fun getEventCode() = PrefHelper(activity).eventCode

    private fun getEventId() = PrefHelper(activity).activityInfoData.data.event.id

    private fun getToken() = TokenBody(PrefHelper(activity).token)

    private fun getPrizeToken() = TokenBody(PrefHelper(activity).prizeToken)

    interface ApiService {


        @POST("sapi/v2/logo/{evCode}")
        fun getEventLogo(@Path("evCode") evCode: String): Call<Any>


        @POST("sapi/v2/login")
        fun getStaffLogin(@Body staffLogin: StaffGetLoginBody): Call<Any>

        @POST("sapi/v2/luckyDrawLogin")
        fun getLuckyDrawLogin(@Body staffLogin: StaffGetLoginBody): Call<Any>

        @POST("sapi/v2/event/{everCode}")
        fun getActivityInfo(@Path("everCode") everCode: String): Call<Any>

        @POST("sapi/v2/event/{everId}/search")
        fun getSearchInfo(@Path("everId") everId: String, @Body searchBody: SearchBody): Call<Any>

        @POST("sapi/v2/event/{everId}/people/{people_id}/checkin")
        fun checkIn(@Path("everId") everId: String, @Path("people_id") people_id: String, @Body tokenBody: TokenBody): Call<Any>

        @POST("sapi/v2/event/{everId}/prize")
        fun getPrizeList(@Path("everId") everId: String): Call<Any>

        @POST("sapi/v2/event/{everId}/prize/{prizeId}/winner")
        fun getPrizeWinner(@Path("everId") everId: String, @Path("prizeId") prizeId: String): Call<Any>

        @POST("sapi/v2/event/{everId}/prize/{prizeId}/draw")
        fun getPrizeDraw(@Path("everId") everId: String, @Path("prizeId") prizeId: String, @Body tokenBody: TokenBody): Call<Any>

        @POST("sapi/v2/event/{everId}/prize/{prizeId}/drawall")
        fun getPrizeDrawAll(@Path("everId") everId: String, @Path("prizeId") prizeId: String, @Body tokenBody: TokenBody): Call<Any>
    }

    private fun bodyToString(request: RequestBody?): String {
        try {
            val buffer = Buffer()
            if (request != null)
                request.writeTo(buffer)
            else
                return ""
            return buffer.readUtf8()
        } catch (e: IOException) {
            return "did not work"
        }
    }
}