package com.check.www.lottery

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import com.check.www.BaseActivity
import com.check.www.R
import com.check.www.adapter.CarouselPagerAdapter
import com.check.www.adapter.LotteryAdapter
import com.check.www.data.LotteryData
import com.check.www.data.LotteryWinnerData
import com.check.www.http.RetrofitHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_winning_lottery.btn_previousPage
import kotlinx.android.synthetic.main.activity_winning_lottery.indicator
import kotlinx.android.synthetic.main.activity_winning_lottery.ll_tab
import kotlinx.android.synthetic.main.activity_winning_lottery.ll_winning
import kotlinx.android.synthetic.main.activity_winning_lottery.tv_count
import kotlinx.android.synthetic.main.activity_winning_lottery.tv_name
import kotlinx.android.synthetic.main.activity_winning_lottery.tv_title
import kotlinx.android.synthetic.main.activity_winning_lottery.vp_carousel

class WinningLotteryActivity : BaseActivity() {

    var regularDataSources: List<LotteryData.Data.Prize> = listOf()
    var extraDataSources: List<LotteryData.Data.Prize> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_winning_lottery)
        getPrizeList()

        btn_previousPage.setOnClickListener {
            finish()
        }
    }


    private fun getPrizeList() {

        showLoading()

        RetrofitHelper(this).setCallback { it ->
            hideLoading()
            if (it.isSuccess) {

                val data = Gson().fromJson<LotteryData>(it.dataBodyJsonString)

                regularDataSources =
                    data.data.prizes.filter { prizeData -> prizeData.prizeType == "regular" }.sortedBy { it.prizeOrder }
                extraDataSources =
                    data.data.prizes.filter { prizeData -> prizeData.prizeType == "extra" }.sortedBy { it.prizeOrder }
                ll_tab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
                    override fun onTabSelected(tab: TabLayout.Tab?) {
                        Log.d("sherry", "tab?.position${tab?.position}")
                        when (tab?.position) {
                            0 -> {
                                Log.d("sherry", "position：0")
                                vp_carousel.clearOnPageChangeListeners()
                                setViewPager(regularDataSources)

                            }
                            1 -> {
                                Log.d("sherry", "position：1")
                                vp_carousel.clearOnPageChangeListeners()
                                setViewPager(extraDataSources)

                            }

                        }

                    }

                    override fun onTabUnselected(tab: TabLayout.Tab?) {
                    }

                    override fun onTabReselected(tab: TabLayout.Tab?) {
                    }
                })

                setViewPager(regularDataSources)

            } else {
                toast("失敗")
            }

        }.getPrizeList()
    }


    private fun setViewPager(data: List<LotteryData.Data.Prize>) {

        setDesc(data)

        vp_carousel.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(position: Int) {

                setDesc(data, position)
            }

            override fun onPageScrollStateChanged(p0: Int) {
            }

        })

        vp_carousel.adapter = CarouselPagerAdapter(this, data)
        indicator.setViewPager(vp_carousel)

    }


    @SuppressLint("SetTextI18n")
    private fun setDesc(data: List<LotteryData.Data.Prize>, position: Int = 0) {

        tv_name.text = data[position].prizename
        tv_title.text = data[position].prizeContent
        tv_count.text = "${data[position].numDrawn}人"
        val adapter = LotteryAdapter(this, data[position].winners)
        ll_winning.layoutManager = LinearLayoutManager(this)
        ll_winning.adapter = adapter


    }

}