package com.check.www.lottery


import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.check.www.BaseFragment
import com.check.www.MainActivity
import com.check.www.R
import com.check.www.app_setting.NetworkSuccessful
import com.check.www.data.LoginData
import com.check.www.data.body.StaffGetLoginBody
import com.check.www.http.RetrofitHelper
import com.check.www.utility.PrefHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.fragment_lottery.view.*


class LotteryFragment : BaseFragment() {

    lateinit var rootView: View

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_lottery, container, false)


        rootView.btn_StartLottery.setOnClickListener {

            val memberData = StaffGetLoginBody.getEmpty()

            memberData.username = rootView.ed_username.text.toString()
            memberData.password = rootView.ed_password.text.toString()


            goLogin(memberData)

        }

        rootView.btn_WinningLottery.setOnClickListener {

            val intent = Intent()
            intent.setClass(context, WinningLotteryActivity::class.java)
            startActivity(intent)

        }

        return rootView
    }


    private fun goLogin(its: StaffGetLoginBody) {

        showLoading()

        RetrofitHelper(activity).setCallback {
            hideLoading()
            if (it.isSuccess) {

                val data = Gson().fromJson<LoginData>(it.dataBodyJsonString)

                if (data.result == NetworkSuccessful) {

                    PrefHelper(activity).prizeToken = data.data.token

                    val intent = Intent()
                    intent.setClass(context, StartLotteryActivity::class.java)
                    startActivity(intent)
                } else {
                    toast("失敗")
                }

            } else {
                toast("失敗")
            }


        }.getLuckyDrawLogin(its)
    }


}
