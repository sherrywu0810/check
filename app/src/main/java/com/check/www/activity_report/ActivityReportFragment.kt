package com.check.www.activity_report


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.check.www.R
import com.check.www.adapter.ActivityReportAdapter
import com.check.www.data.ActivityReportData
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.fragment_activity_report.view.*


class ActivityReportFragment : Fragment() {
    val dataSource: MutableList<ActivityReportData> = mutableListOf()
    lateinit var rootView: View
    private var linearLayoutManager: LinearLayoutManager? = null


    companion object {

        var type = ActivityReportData.onces

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_activity_report, container, false)
        dataSource.add(ActivityReportData("單人掃描", "點擊一次後，將自動不間斷進行 下一組 QR Code掃描",R.drawable.single_scan,ActivityReportData.onces))
        dataSource.add(ActivityReportData("連續掃描", "點擊一次後，將自動不間斷進行 下一組 QR Code掃描",R.drawable.continuous_scan,ActivityReportData.continuous))

        val adapter = ActivityReportAdapter(context, dataSource)
        linearLayoutManager = LinearLayoutManager(context)
        linearLayoutManager?.orientation = LinearLayoutManager.HORIZONTAL
        rootView.ll_ActivityReport.adapter = adapter
        rootView.ll_ActivityReport.layoutManager = linearLayoutManager

        adapter.setOnItemClickListener(object : ActivityReportAdapter.OnItemClickListener {
            override fun onEnterClick(position: Int) {
                type = dataSource[position].type
                when (type) {
                    ActivityReportData.onces -> {
                        IntentIntegrator(activity).setCaptureActivity(ScanActivity::class.java).initiateScan()

                    }
                    ActivityReportData.continuous -> {
                        IntentIntegrator(activity).setCaptureActivity(ScanActivity::class.java).initiateScan()
                    }


                }
            }

        })

        return rootView
    }


}
