package com.check.www.activity_report

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.KeyEvent

import com.check.www.R
import com.journeyapps.barcodescanner.CaptureManager
import kotlinx.android.synthetic.main.activity_scan.*


class ScanActivity : AppCompatActivity() {


    private var capture: CaptureManager? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan)


        capture = CaptureManager(this, bv_barcode)

        capture?.initializeFromIntent(intent, savedInstanceState)
        capture?.decode()



        btn_previousPage.setOnClickListener {
            finish()

        }
        btn_previousPage2.setOnClickListener {
            finish()
        }


    }

    override fun onResume() {
        super.onResume()
        capture?.onResume()
    }

    override fun onPause() {
        capture?.onPause()
        finish()
        super.onPause()

    }

    override fun onDestroy() {
        capture?.onDestroy()
        super.onDestroy()

    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        capture?.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        capture?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onKeyDown(keyCode: Int, event: KeyEvent?): Boolean {
        return bv_barcode.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event)
    }




}
