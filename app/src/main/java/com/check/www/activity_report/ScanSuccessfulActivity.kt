package com.check.www.activity_report

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.FragmentActivity
import android.util.Log
import android.view.View
import android.widget.Toast
import com.check.www.BaseActivity
import com.check.www.R
import com.check.www.data.ActivityInfoData
import com.check.www.data.ActivityReportData
import com.check.www.data.PeopleGetCheckIn
import com.check.www.http.RetrofitHelper
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_scan_successful.*
import kotlinx.android.synthetic.main.cell_manualreport_item.view.*
import java.sql.Time
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Delayed
import kotlin.concurrent.schedule


class ScanSuccessfulActivity : BaseActivity() {

    @SuppressLint("SimpleDateFormat")

    val handler = Handler()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scan_successful)

        val checkInpcode = intent?.extras?.getString("checkInpcode")

        if (checkInpcode != null) {

            checkIn(checkInpcode)

        }

        btn_previousPage.setOnClickListener {
            finish()
        }





        if (ActivityReportFragment.type == ActivityReportData.onces) {
            sec_title.text = "一次掃描"
            btn_enter.visibility = View.VISIBLE
            btn_enter.setOnClickListener {
                IntentIntegrator(this).setCaptureActivity(ScanActivity::class.java).initiateScan()

            }

        } else {
            btn_enter.visibility = View.INVISIBLE
            sec_title.text = "連續掃描"
        }


    }

    override fun onPause() {
        super.onPause()

        handler.removeMessages(0)


    }


    override fun onResume() {
        super.onResume()



    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                finish()
                Toast.makeText(this, "掃描結果為空的", Toast.LENGTH_LONG).show()
            } else {


                checkIn(result.contents)


                Log.d("sherry", "onActivityResult")
                Toast.makeText(this, result.contents, Toast.LENGTH_LONG).show()


            }
        }

    }


    private fun checkIn(pcode: String) {

        tv_date.text = ""

        tv_department.text = ""
        tv_name.text = ""
        tv_pcode.text = ""
        tv_unit.text = ""
        tv_status.text = ""


        showLoading()

        RetrofitHelper(this).setCallback {
            hideLoading()
            if (it.isSuccess) {

                val peopleGetCheckInData = Gson().fromJson<PeopleGetCheckIn>(it.dataBodyJsonString)
                tv_date.text = peopleGetCheckInData.data.person.checkinTS
                // tv_time.text = peopleGetCheckInData.data.checkinTime
                tv_department.text = peopleGetCheckInData.data.person.department
                tv_name.text = peopleGetCheckInData.data.person.name
                tv_pcode.text = peopleGetCheckInData.data.person.pcode
                tv_unit.text = peopleGetCheckInData.data.person.unit

                if (peopleGetCheckInData.data.person.firstTime) {
                    tv_status.text = "報到成功"
                }else{
                    tv_status.text = "重複報到"
                }


                if (ActivityReportFragment.type == ActivityReportData.continuous)

                    handler.postDelayed({
                        IntentIntegrator(this).setCaptureActivity(ScanActivity::class.java).initiateScan()

                    }, 3000)


            } else {
                tv_welcome.visibility = View.INVISIBLE
                tv_status.text = "報到失敗"

                if (ActivityReportFragment.type == ActivityReportData.continuous)

                    handler.postDelayed({
                        IntentIntegrator(this).setCaptureActivity(ScanActivity::class.java).initiateScan()

                    }, 3000)

                toast("報到失敗")
            }
        }.checkIn(pcode)



    }


}
