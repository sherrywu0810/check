package com.check.www

import android.support.v4.app.Fragment
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.activity_base.*


open class BaseFragment : Fragment() {

    inline fun <reified T> Gson.fromJson(json: String) = this.fromJson<T>(json, object : TypeToken<T>() {}.type)

    fun toast(msg: String) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }


    fun showLoading(){
        activity?.fl_base_loading?.visibility = View.VISIBLE
    }
    fun hideLoading(){
        activity?.fl_base_loading?.visibility = View.GONE
    }
}
