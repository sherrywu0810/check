package com.check.www

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.bumptech.glide.Glide
import com.check.www.app_setting.NetworkSuccessful
import com.check.www.data.LoginData
import com.check.www.data.LogoData
import com.check.www.data.body.StaffGetLoginBody
import com.check.www.http.RetrofitHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : BaseActivity() {

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (BuildConfig.DEBUG) {
            //  edit_name.setText("user1")
            edit_no.setText("VU190101A")
        }


        edit_name.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                Log.d("sherry", "setOnFocusChangeListener")
                getLogo(edit_no.text.toString())
            }

        }

        btn_login.setOnClickListener {

            val memberData = StaffGetLoginBody.getEmpty()

            memberData.username = edit_name.text.toString()
            memberData.password = edit_no.text.toString()

            goLogin(memberData)

        }


    }

    private fun getLogo(eventCode: String) {


        RetrofitHelper(this).setCallback {
            hideLoading()
            if (it.isSuccess) {
                val data = Gson().fromJson<LogoData>(it.dataBodyJsonString)
                Glide.with(this).load(data.data.logo).into(img_logo)
            }

        }.getEventLogo(eventCode)


    }


    private fun goLogin(its: StaffGetLoginBody) {

        showLoading()

        RetrofitHelper(this).setCallback {
            hideLoading()
            if (!it.isSuccess) {
                Toast.makeText(this, getString(R.string.network_fail), Toast.LENGTH_SHORT).show()
                return@setCallback
            }
            val data = Gson().fromJson<LoginData>(it.dataBodyJsonString)

            if (data.result != NetworkSuccessful) {
                Toast.makeText(this, getString(R.string.login_fail), Toast.LENGTH_SHORT).show()
                return@setCallback

            }
            preHelper.token = data.data.token
            preHelper.eventCode = its.password

            val intent = Intent()
            intent.setClass(this, MainActivity::class.java)
            startActivity(intent)
            finish()

        }.getStaffLogin(its)
    }
}

//    val its = """{
//  "result": 1,
//  "errormsg": "success",
//  "data": {
//    "event":{
//      "eventCode": "VC3321230",
//      "eventName": "2019 winner seminar"
//    },
//    "stats": {
//      "percent": 20,
//      "numCheckin": 100,
//      "notCheckin": 400,
//     "total": 500
//    },
//    "cookie": "checkin helper cookie"
//  },
//  "time": 1572226226241,
//  "hash": "md5()"
//}  """