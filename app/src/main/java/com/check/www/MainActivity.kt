package com.check.www

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.check.www.activity_report.ActivityReportFragment
import com.check.www.activity_report.ScanSuccessfulActivity
import com.check.www.app_setting.NetworkSuccessful
import com.check.www.data.ActivityInfoData
import com.check.www.home.HomeFragment
import com.check.www.http.RetrofitHelper
import com.check.www.lottery.LotteryFragment
import com.check.www.manual_report.ManualReportFragment
import com.google.gson.Gson
import com.google.zxing.integration.android.IntentIntegrator
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getActivityInfo()
//        val loginData = PrefHelper.loginData.data
//        tv_title.text = loginData.event.eventName

        home.setOnClickListener {
            setBottomButton(it)
            sec_title.text = preHelper.activityInfoData.data.event.eventCode

            img_backgrand.visibility = View.VISIBLE
            ll_title.layoutParams.height = ((ll_title.context.resources.displayMetrics.density) * 240).toInt()
            sec_title.setBackgroundResource(R.drawable.sec_title_background_blue)
            replaceFragment(HomeFragment(), R.id.containerFrameLayout)
        }
        manual_report.setOnClickListener {
            setBottomButton(it)
            setTitle("手動查詢")
            img_backgrand.visibility = View.GONE
            ll_title.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            sec_title.setBackgroundResource(R.drawable.sec_title_background_white)
            replaceFragment(ManualReportFragment(), R.id.containerFrameLayout)


        }
        activity_report.setOnClickListener {
            setBottomButton(it)
            setTitle("活動報到")
            img_backgrand.visibility = View.GONE
            ll_title.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            sec_title.setBackgroundResource(R.drawable.sec_title_background_white)
            replaceFragment(ActivityReportFragment(), R.id.containerFrameLayout)

        }
        lottery.setOnClickListener {
            setBottomButton(it)
            setTitle("活動抽獎")
            img_backgrand.visibility = View.GONE
            ll_title.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            sec_title.setBackgroundResource(R.drawable.sec_title_background_white)
            replaceFragment(LotteryFragment(), R.id.containerFrameLayout)
        }


    }


    private fun getActivityInfo() {
        showLoading()
        RetrofitHelper(this).setCallback {

            if (it.isSuccess) {

                val data = Gson().fromJson<ActivityInfoData>(it.dataBodyJsonString)
                if (data.result == NetworkSuccessful) {
                    hideLoading()
                    preHelper.activityInfoData = data
                    Glide.with(this).load(data.data.event.themePic).into(img_backgrand)
                    tv_title.text = data.data.event.eventName
                    sec_title.text = data.data.event.eventCode
                    replaceFragment(HomeFragment(), R.id.containerFrameLayout)

                } else {
                    hideLoading()
                    Toast.makeText(this, "失敗", Toast.LENGTH_SHORT).show()
                }

            } else {
                hideLoading()
                Toast.makeText(this, "失敗", Toast.LENGTH_SHORT).show()
            }
        }.getActivityInfo()
    }


    private fun setBottomButton(view: View) {
        home.setImageResource(R.drawable.index_none)
        manual_report.setImageResource(R.drawable.manual_none)
        activity_report.setImageResource(R.drawable.activity_none)
        lottery.setImageResource(R.drawable.lottery_none)

        when (view) {
            home -> home.setImageResource(R.drawable.index_display)
            manual_report -> manual_report.setImageResource(R.drawable.manual_display)
            activity_report -> activity_report.setImageResource(R.drawable.activity_display)
            lottery -> lottery.setImageResource(R.drawable.lottery_display)


        }


    }


    private fun addFragment(fragment: Fragment, tag: Int) {
        val manager = supportFragmentManager
        val ft = manager.beginTransaction()

        ft.addToBackStack(tag.toString())
        ft.replace(R.id.containerFrameLayout, fragment, tag.toString())
        ft.commitAllowingStateLoss()
    }


    private fun AppCompatActivity.replaceFragment(fragment: Fragment, frameId: Int) {
        supportFragmentManager.inTransaction { replace(frameId, fragment) }


    }


    private inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {

        beginTransaction().func().commitAllowingStateLoss()
    }


    private fun setTitle(string: String) {

        sec_title.text = string
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)


        var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {

                Toast.makeText(this, "掃描結果為空", Toast.LENGTH_LONG).show()
            } else {
                val intent = Intent()
                intent.setClass(this, ScanSuccessfulActivity::class.java)
                intent.putExtra("checkInpcode", result.contents)
                startActivity(intent)

            }
        }

    }

    private var firstPressedTime: Long = 0L

    override fun onBackPressed() {

        if (System.currentTimeMillis() - firstPressedTime < 2000) {
            super.onBackPressed()
        } else {

            Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show()
            firstPressedTime = System.currentTimeMillis()
        }
    }
}