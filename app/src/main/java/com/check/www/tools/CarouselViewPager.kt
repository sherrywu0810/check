package com.check.www.tools

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import com.check.www.R
import com.check.www.tools.pager_transformer.CarouselEffectTransformer


class CarouselViewPager @JvmOverloads  constructor(
    context: Context,
    attrs: AttributeSet? = null): ViewPager(context,attrs){

    init {
        val paddingSize = resources.getDimensionPixelOffset(R.dimen.pager_margin)
        clipChildren = false
        pageMargin = paddingSize
        clipToPadding = false
        setPadding(paddingSize * 2, 0, paddingSize * 2, 0)
        offscreenPageLimit = 3
        setPageTransformer(false, CarouselEffectTransformer(context))
    }

}
