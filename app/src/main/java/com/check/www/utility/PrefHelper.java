package com.check.www.utility;

import android.content.Context;
import android.content.SharedPreferences;
import com.check.www.data.ActivityInfoData;
import com.check.www.data.LoginData;

public class PrefHelper {

    public  Context context;

    private static final String prfMember = "prfMember";
    private static String token = "";
    public static String loginData = "";
    private static String eventCode = "";
    private static String prizeToken = "";
    private static ActivityInfoData activityInfoData  ;


    private static final String cookie = "cookie";
    public PrefHelper(Context context){
        this.context = context;
    }


//    public String getToken() {
//      return context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).getString(token, "");
//    }
//
//    public void setToken(String eventCodes) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).edit();
//        editor.putString(token, eventCodes);
//        editor.apply();
//    }



    public String getPrizeToken() {
        return prizeToken;
    }

    public void  setPrizeToken(String tokens) {

        prizeToken = tokens;

    }


    public String getToken() {
        return token;
    }

    public void  setToken(String tokens) {

        token = tokens;

    }

    public String getEventCode() {
        return eventCode;
    }

    public void setEventCode(String eventCodes) {

        eventCode = eventCodes;

    }

    public ActivityInfoData getActivityInfoData() {
        return activityInfoData;
    }

    public void  setActivityInfoData(ActivityInfoData activityInfoDatas) {

        activityInfoData = activityInfoDatas;

    }









//    public String getRefreshToken() {
//        return context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).getString(keyUserRefreshToken, "");
//    }
//
//    public void setRefreshToken(String refreshToken) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).edit();
//        editor.putString(keyUserRefreshToken, refreshToken);
//        editor.apply();
//    }
//
//
//    public boolean isKeepLoginState() {
//        return context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).getBoolean(keyKeepLogin, true);
//    }
//
//    public void setKeepLogin(boolean isKeep) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).edit();
//        editor.putBoolean(keyKeepLogin, isKeep);
//        editor.apply();
//    }
//
//
//    public boolean isRemberPhone() {
//        return context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).getBoolean(keyRememberPhone, true);
//    }
//
//    public void setRemberPhone(boolean isKeep) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).edit();
//        editor.putBoolean(keyRememberPhone, isKeep);
//        editor.apply();
//    }
//    public void setMemberPhone(String phone) {
//        SharedPreferences.Editor editor = context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).edit();
//        editor.putString(keyMemberPhone, phone);
//        editor.apply();
//    }
//
//    public String getMemberPhone() {
//        return context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).getString(keyMemberPhone, "");
//
//    }
//
//
//    public void clearAll() {
//        SharedPreferences.Editor editor = context.getSharedPreferences(prfMember, Context.MODE_PRIVATE).edit();
//        editor.clear().apply();
//    }
}
