package com.check.www.utility;

public class FormatHelper {


    public static  String getFloat(String string, Integer integer) {

        float f = Float.parseFloat(string);
        return String.format("%."+integer+"f", f);
    }
}
