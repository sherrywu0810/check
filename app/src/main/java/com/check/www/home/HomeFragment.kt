package com.check.www.home


import android.app.Activity
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Paint.ANTI_ALIAS_FLAG
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.check.www.BaseFragment
import com.check.www.MainActivity
import com.check.www.R
import com.check.www.app_setting.NetworkSuccessful
import com.check.www.data.ActivityInfoData
import com.check.www.data.body.TokenBody
import com.check.www.http.RetrofitHelper
import com.check.www.utility.PrefHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import com.check.www.utility.PrefHelper.loginData as loginData1


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class HomeFragment : BaseFragment() {
    private lateinit var rootView: View
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        rootView = inflater.inflate(R.layout.fragment_home, container, false)
        rootView.percent.setOnCenterDraw { canvas, rectF, x, y, storkeWidth, progress ->
            val textPaint = Paint(ANTI_ALIAS_FLAG)
            textPaint.color = Color.parseColor("#FFFFFF")
            textPaint.textSize = 100.0f
            val progressStr = "$progress%"
            val textX = x - textPaint.measureText(progressStr) / 2
            val textY = y - (textPaint.descent() + textPaint.ascent()) / 2
            canvas?.drawText(progressStr, textX, textY, textPaint)
        }
        return rootView
    }

    override fun onResume() {
        super.onResume()
        getActivityInfo()
    }


    private fun getActivityInfo() {

        showLoading()
        RetrofitHelper(activity).setCallback {

            if (it.isSuccess) {
//                toast("成功")
                val data = Gson().fromJson<ActivityInfoData>(it.dataBodyJsonString)
                if (data.result == NetworkSuccessful) {

                    PrefHelper(context).activityInfoData = data

                    rootView.percent.progress = data.data.event.percent.toInt()
                    rootView.tv_numCheckin.text = data.data.event.numCheckin.toString()
                    rootView.tv_notCheckin.text = (data.data.event.numTotal - data.data.event.numCheckin).toString()
                    rootView.tv_total.text = data.data.event.numTotal.toString()
                    hideLoading()

                } else {
                    hideLoading()
                      toast("失敗")
                }

            } else {
                hideLoading()
                  toast("失敗")
            }
        }.getActivityInfo()
    }


}
