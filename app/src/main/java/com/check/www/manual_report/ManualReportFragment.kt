package com.check.www.manual_report


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.check.www.BaseFragment
import com.check.www.R
import com.check.www.adapter.ManualReportAdapter
import com.check.www.app_setting.NetworkSuccessful
import com.check.www.data.ManualReportData
import com.check.www.data.body.SearchBody
import com.check.www.http.RetrofitHelper
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_manual_report.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */

class ManualReportFragment : BaseFragment() {
    private val dataSource: MutableList<ManualReportData> = mutableListOf()
    lateinit var rootView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_manual_report, container, false)


        val adapter = ManualReportAdapter(context)

        rootView.ll_ManualReport.adapter = adapter
        rootView.ll_ManualReport.layoutManager = LinearLayoutManager(context)


        rootView.btn_search.setOnClickListener {
            showLoading()

            val tokenSearchBody= SearchBody(rootView.tv_search.text.toString())

            RetrofitHelper(activity).setCallback {
                hideLoading()
                if (it.isSuccess) {
                    val data = Gson().fromJson<ManualReportData>(it.dataBodyJsonString)
                    if(data.result== NetworkSuccessful) {
                        adapter.setManualReportData(data.data.people)
                        adapter.notifyDataSetChanged()

                    } else {
                      toast("失敗")
                    }

                } else {
                  toast("失敗")
                }
            }.getSearchInfo(tokenSearchBody)





        }


        return rootView

    }


}
